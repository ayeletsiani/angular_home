import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  
  
  
  constructor(private booksservice: BooksService) { }
  name;
  id;

  nameOfAuthor: any;
  nameOfAuthor$:Observable<any>;
  ngOnInit() {
    this.nameOfAuthor$ = this.booksservice.getAuthor();
       }
  
  onSubmit(){
    this.booksservice.addAuthor(this.name)
        
      }
}
