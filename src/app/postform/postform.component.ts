import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {
  textButton:string = 'Add Post'; 
  headline:string = 'Add New Post'
  name:string;
  author:string;
  body: string;
  id: string;
  isEdit: boolean = false;
  userId:string;
  [x: string]: any;
  constructor(private postservice: PostsService, private router: Router, private route:ActivatedRoute, private auth: AuthService) { }
  
  
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.auth.user.subscribe(
          user => 
          {
            this.userId = user.uid;
 
              if(this.id)
            {
              this.isEdit = true;
              this.textButton = "Update Post";
              this.headline = "Update Your Post";
              this.postservice.getPost(this.userId, this.id).subscribe
              (
                post => 
                {
                  console.log(this.name);
                  this.name=post.data().name;
                  this.author=post.data().author;
                  this.body=post.data().body;
                }
              )
            }
          }
    )
    
  }
  onSubmit(){
    if(this.isEdit) {
      this.postservice.updatPost(this.userId,this.id, this.name, this.author ,this.body);
      this.router.navigate(['/posts'])
    }
  
    else{
      this.postservice.addPosts(this.userId,this.name, this.author ,this.body);
      this.router.navigate(['/posts'])
    }
   
  }
}
