import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//angular material
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';

import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule, Routes } from '@angular/router';
import { PostsService } from './posts.service';
import { HttpClientModule } from '@angular/common/http';

//components
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { PostformComponent } from './postform/postform.component';
import { PostsComponent } from './posts/posts.component';
import { BooksComponent } from './books/books.component';
import { AuthorsComponent } from './authors/authors.component';

// Firebase
import { AngularFireModule ,} from '@angular/fire';
import { environment } from '../environments/environment';

// Firebase modules
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AngularFireAuth } from '@angular/fire/auth';

const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors',      component: AuthorsComponent },
  { path: '', redirectTo: '/books' ,pathMatch: 'full'},
  { path: 'posts',      component: PostsComponent },
  { path: 'postform',      component: PostformComponent },
  { path: 'postform/:id',      component: PostformComponent },
  { path: 'signup',      component: SignupComponent },
  { path: 'login',      component: LoginComponent },
];



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    PostsComponent,
    PostformComponent,
    SignupComponent,
    LoginComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatFormFieldModule,
    MatListModule,
    RouterModule,
    // import HttpClientModule after BrowserModule.
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'angular-home'),
    AngularFireStorageModule,
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
