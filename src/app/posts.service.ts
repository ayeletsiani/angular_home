import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClientModule, HttpClient} from '@angular/common/http';  
import { AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  postCollection:AngularFirestoreCollection;
  //urlpost = "https://jsonplaceholder.typicode.com/posts"
  //urluser = "https://jsonplaceholder.typicode.com/users"
  
  constructor(private http: HttpClient, private db:AngularFirestore) { }
  
  getPosts(userId:string):Observable<any[]>
    {
      //return this.db.collection('posts').valueChanges({idField:'id'});
      
            this.postCollection = this.db.collection(`users/${userId}/posts`);
            return this.postCollection.snapshotChanges().pipe
            (
               map
               (
                collection => collection.map
                (
                  document=> 
                  {
                    const data = document.payload.doc.data();
                    console.log('document');
                    data.id = document.payload.doc.id;
                    return data;
                  }
                ) 
               )
             )
    }
  
  
  
  getPost(userId:string, id:string):Observable<any>{
    //return this.db.doc(`posts/${id}`).get();
    return this.db.doc(`users/${userId}/posts/${id}`).get();
  }
  

  
 
  addPosts(userId:string, name:string, author:string, body:string){
      const post = { name:name,author:author, body:body};
      //this.db.collection('posts').add(post);
      this.userCollection.doc(userId).collection('posts').add(post); //הוספה של הספר בתוך האוסף של היוזרים

      }
      
  updatPost(userId:string,id:string, name:string, author:string ,body:string){
    const post ={name:name ,author:author ,body:body};
    this.db.doc(`users/${userId}/posts/${id}`).update(post);
  }



  deletePost(userId:string, id:string)
      {
        //this.db.doc(`posts/${id}`).delete();
        this.db.doc(`users/${userId}/posts/${id}`).delete();
      }
     

  


  }
