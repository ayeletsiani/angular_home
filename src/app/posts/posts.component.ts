import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  constructor(private postservice: PostsService, private auth: AuthService) { }
  
  listOfPosts$:Observable<any>;
  //listOfUsers$:User[]=[];
  name:string;
  body:string;
  author:string;
  check:string;
  userId:string; 
  panelOpenState = false;
  listOfPosts: any;

  ngOnInit() {
   
    //this.listOfPosts$ = this.postservice.getPosts();
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.listOfPosts$ = this.postservice.getPosts(this.userId);
        console.log('oninit work');
      }
    )
       
  }
  
  deletePost(id:string){
    this.postservice.deletePost(this.userId, id);
  }
  /*saveToFb(){
          
          for (let index = 0; index < this.listOfPosts$.length; index++) {
            for (let i = 0; i < this.listOfUsers$.length; i++)
            {
              if (this.listOfPosts$[index].userId==this.listOfUsers$[i].id) {
                console.log('1');
                this.title = this.listOfPosts$[index].title;
                this.body = this.listOfPosts$[index].body;
                this.author = this.listOfUsers$[i].name;
                this.postservice.addPosts(this.body,this.author,this.title);
                this.check ="The data was successfully uploaded to Firebase";
              }
              
            }
          }
          
        } */
      


  }

