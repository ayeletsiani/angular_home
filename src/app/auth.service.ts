import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interface/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>
  
  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState //פייר בייס ינהל את היוזרים
  }

  signUp(email:string, password:string)
  {
    this.afAuth
        .auth.createUserWithEmailAndPassword(email,password)
        .then(res => console.log('Succesful Signup', res)) //"then" הפונקציה 
                                                            //מחזירה פורמיס
                                                            // כמו סאבסקרייב של אובזראבל
  }

 logOut(){
   this.afAuth.auth.signOut().then(res => console.log('Succesful Logout', res));
 }

 logIn(email:string,password:string){
  console.log(email);
  this.afAuth.auth.signInWithEmailAndPassword(email,password).then(res => console.log('Succesful Login', res)) //"then" הפונקציה 
                                                              //מחזירה פורמיס
                                                              // כמו סאבסקרייב של אובזראבל;
}
}
